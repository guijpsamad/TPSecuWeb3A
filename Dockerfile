# Copyright {2023} {Viardot Sebastien}
# Pour créer l'image docker build . -t tpsecuweb
# Image de base
FROM debian:latest
# Auteur
MAINTAINER Sebastien Viardot <Sebastien.Viardot@grenoble-inp.fr>
# Installe le nécessaire pour disposer du framework play pour le TP
RUN apt-get update && \
    apt-get -y install wget python3 ant git unzip &&\
    cd /tmp &&\
    wget https://github.com/playframework/play1/releases/download/1.7.1/play-1.7.1.zip &&\
    unzip play-1.7.1.zip && mv play-1.7.1 /opt/play
RUN chmod +x /opt/play/play
VOLUME /source
EXPOSE 9000
WORKDIR /source
